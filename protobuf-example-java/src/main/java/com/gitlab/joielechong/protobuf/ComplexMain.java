package com.gitlab.joielechong.protobuf;

import example.complex.Complex;

import java.util.Arrays;

public class ComplexMain {
    public static void main(String[] args) {
        System.out.println("Complex example");

        Complex.DummyMessage oneDummy = newDummyMessage(1, "one dummy");

        Complex.ComplexMessage.Builder builder = Complex.ComplexMessage.newBuilder();
        builder.setOneDummy(oneDummy);

        builder.addMultipleDummy(newDummyMessage(2, "second dummy"));
        builder.addMultipleDummy(newDummyMessage(3, "third dummy"));
        builder.addMultipleDummy(newDummyMessage(4, "fourth dummy"));

        builder.addAllMultipleDummy(Arrays.asList(
                newDummyMessage(5, "fifth dummy"),
                newDummyMessage(6, "sixth dummy")
        ));

        Complex.ComplexMessage message = builder.build();
        System.out.println(message.toString());
    }

    private static Complex.DummyMessage newDummyMessage(Integer id, String name) {
        Complex.DummyMessage.Builder builder = Complex.DummyMessage.newBuilder();

        return builder.setName(name)
                .setId(id)
                .build();
    }
}
