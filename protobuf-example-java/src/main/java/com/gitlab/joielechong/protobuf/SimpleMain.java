package com.gitlab.joielechong.protobuf;


import com.example.simple.Simple;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

public class SimpleMain {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        Simple.SimpleMessage.Builder builder = Simple.SimpleMessage.newBuilder();

        // simple fields
        builder.setId(1)
                .setIsSimple(true)
                .setName("Simple message");

        // repeated fields
        builder.addSampleList(0);
        builder.addSampleList(1);
        builder.addAllSampleList(Arrays.asList(2,3,4,5,6,7));

        System.out.println(builder.toString());

        Simple.SimpleMessage message = builder.build();

        // write protobuf binary to a file
        System.out.println("Save to file");
        try (FileOutputStream outputStream = new FileOutputStream("simple_message.dat")) {
            message.writeTo(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println();
        System.out.println("Print from file");
        try (FileInputStream inputStream = new FileInputStream("simple_message.dat")) {
            Simple.SimpleMessage messageFromFile = Simple.SimpleMessage.parseFrom(inputStream);
            System.out.println(messageFromFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
