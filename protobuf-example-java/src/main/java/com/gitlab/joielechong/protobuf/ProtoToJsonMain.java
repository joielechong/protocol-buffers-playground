package com.gitlab.joielechong.protobuf;

import com.example.simple.Simple;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;

import java.util.Arrays;

public class ProtoToJsonMain {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        Simple.SimpleMessage.Builder builder = Simple.SimpleMessage.newBuilder();

        // simple fields
        builder.setId(1)
                .setIsSimple(true)
                .setName("Simple message");

        // repeated fields
        builder.addSampleList(0);
        builder.addSampleList(1);
        builder.addAllSampleList(Arrays.asList(2, 3, 4, 5, 6, 7));

        try {
            String json = JsonFormat.printer()
//                   .includingDefaultValueFields() // option
                    .print(builder);
            System.out.println(json);
            System.out.println("\n-----------------------\n");
            System.out.println("Parse JSON into protobuf");
            // WARNING!! Don't use this in production!!!
            Simple.SimpleMessage.Builder builder2 = Simple.SimpleMessage.newBuilder();
            JsonFormat.parser()
                    .ignoringUnknownFields()
                    .merge(json, builder2);

            System.out.println(builder2);

        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
    }
}
