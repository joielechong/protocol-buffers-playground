package com.gitlab.joielechong.protobuf;

import example.enum_example.EnumExample;

public class EnumMain {
    public static void main(String[] args) {
        System.out.println("Example for enums");

        EnumExample.EnumMessage.Builder builder = EnumExample.EnumMessage.newBuilder();

        builder.setId(1);

        // Example with enums
        builder.setDayOfWeek(EnumExample.DayOfWeek.FRIDAY);
        EnumExample.EnumMessage message = builder.build();
        System.out.println(message);

    }
}
